<?php

namespace Apeisia\OmnipayBundle;

use Apeisia\OmnipayBundle\DependencyInjection\ApeisiaOmnipayBundleExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApeisiaOmnipayBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new ApeisiaOmnipayBundleExtension();
    }

}
