<?php

namespace Apeisia\OmnipayBundle\Model;

interface GatewayConfigInterface
{
    public function getConfig();
    public function getFactoryName();
    public function isTestMode(): bool;
}
