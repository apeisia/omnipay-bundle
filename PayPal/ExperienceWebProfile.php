<?php

namespace Apeisia\OmnipayBundle\PayPal;

use Apeisia\OmnipayBundle\PayPal\Messages\ExperienceWebProfileCreate;
use Apeisia\OmnipayBundle\PayPal\Messages\ExperienceWebProfileDelete;
use Apeisia\OmnipayBundle\PayPal\Messages\ExperienceWebProfileList;
use Apeisia\OmnipayBundle\PayPal\Messages\ExperienceWebProfileUpdate;
use Illuminate\Support\Arr;
use Omnipay\PayPal\RestGateway;

class ExperienceWebProfile
{
    private RestGateway $gateway;

    public function __construct(RestGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function list()
    {
        return $this->gateway->createRequest(ExperienceWebProfileList::class, [])->send()->getData();
    }

    public function create($data)
    {
        return $this->gateway->createRequest(ExperienceWebProfileCreate::class, ['profile' => $data])->send()
            ->getData()
            ;
    }

    public function update($id, $data)
    {
        return $this->gateway->createRequest(ExperienceWebProfileUpdate::class, [
            'id'      => $id,
            'profile' => $data,
        ])->send()->getData()
            ;
    }

    public function delete($id)
    {
        return $this->gateway->createRequest(ExperienceWebProfileDelete::class, [
            'id' => $id,
        ])->send()
            ;
    }

    public function getIdFor($data)
    {
        foreach ($this->list() as $profile) {
            $diff = $this->arrayRecursiveDiff($profile, $data);
            // these two fields are not in $data
            unset($diff['temporary']);
            unset($diff['id']);
            if (count($diff) == 0) {
                return $profile['id'];
            } else {
                if ($profile['name'] == $data['name']) {
                    $this->delete($profile['id']);
                    break; // go to the create code
                }
            }
        }

        // none found or existing one deleted, create a new one
        return $this->create($data)['id'];
    }

    function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = [];

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }

        return $aReturn;
    }
}
