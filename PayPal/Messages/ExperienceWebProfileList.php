<?php

namespace Apeisia\OmnipayBundle\PayPal\Messages;

use Omnipay\PayPal\Message\AbstractRestRequest;

class ExperienceWebProfileList extends AbstractRestRequest
{

    protected function getHttpMethod()
    {
        return 'GET';
    }

    protected function getEndpoint()
    {
        return parent::getEndpoint() . '/payment-experience/web-profiles';
    }

    public function getData()
    {
        return [];
    }
}
