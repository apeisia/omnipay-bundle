<?php

namespace Apeisia\OmnipayBundle\PayPal\Messages;

use Omnipay\PayPal\Message\AbstractRestRequest;

class ExperienceWebProfileDelete extends AbstractRestRequest
{

    public function getId()
    {
        return $this->getParameter('id');
    }

    public function setId($value)
    {
        return $this->setParameter('id', $value);
    }

    protected function getHttpMethod()
    {
        return 'DELETE';
    }

    protected function getEndpoint()
    {
        return parent::getEndpoint() . '/payment-experience/web-profiles/' . $this->getId();
    }

    public function getData()
    {
        return null;
    }
}
