<?php

namespace Apeisia\OmnipayBundle\PayPal\Messages;

use Omnipay\PayPal\Message\AbstractRestRequest;

class ExperienceWebProfileUpdate extends ExperienceWebProfileCreate
{

    public function getId()
    {
        return $this->getParameter('id');
    }

    public function setId($value)
    {
        return $this->setParameter('id', $value);
    }

    protected function getEndpoint()
    {
        return parent::getEndpoint() . '/' . $this->getId();
    }

    protected function getHttpMethod()
    {
        return 'PUT';
    }
}
