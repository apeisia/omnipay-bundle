<?php

namespace Apeisia\OmnipayBundle\PayPal\Messages;

use Omnipay\PayPal\Message\AbstractRestRequest;

class ExperienceWebProfileCreate extends AbstractRestRequest
{
    protected function getEndpoint()
    {
        return parent::getEndpoint() . '/payment-experience/web-profiles';
    }

    public function getData()
    {
        return $this->getParameter('profile');
    }

    public function getProfile()
    {
        return $this->getParameter('profile');
    }

    public function setProfile($value)
    {
        return $this->setParameter('profile', $value);
    }
}
