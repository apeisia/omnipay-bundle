<?php

namespace Apeisia\OmnipayBundle\Service;

use Apeisia\OmnipayBundle\Model\GatewayConfigInterface;
use Apeisia\OmnipayBundle\PayPal\ExperienceWebProfile;
use Omnipay\Common\GatewayInterface;
use Omnipay\Omnipay;
use Omnipay\PayPal\RestGateway;
use Omnipay\Sofort;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class OmnipayGateway
{
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function preparePurchase(GatewayConfigInterface $gatewayConfig, $params, $sessionIdentifier, $paypalExperienceProfile = null)
    {
        $gateway = $this->initGateway($gatewayConfig);

        if ($gateway instanceof RestGateway && $paypalExperienceProfile) { // PayPal

            $profile = new ExperienceWebProfile($gateway);

            $params['experience_profile_id'] = $profile->getIdFor($paypalExperienceProfile);
        }
        $response                       = $gateway->purchase($params)->send();
        $params['transactionReference'] = $response->getTransactionReference();

        $this->session->set('payment.' . $sessionIdentifier, $params);

        return $response;
    }

    public function completePurchase(GatewayConfigInterface $gatewayConfig, Request $request, $sessionIdentifier)
    {

        $gateway      = $this->initGateway($gatewayConfig);
        $query2params = [];
        if ($gateway instanceof RestGateway) {
            $query2params = [
                // PayPal
                'PayerID' => 'payerId',

            ];
        }
        $sessKey = 'payment.' . $sessionIdentifier;
        if(!$this->session->has($sessKey)) {
            return null;
        }
        $params = $this->session->get($sessKey);
        $this->session->remove($sessKey);
        $params['clientIp'] = $request->getClientIp();

        foreach ($query2params as $query => $param) {
            if ($request->query->has($query)) {
                $params[$param] = $request->query->get($query);
            }
        }

        if ($gateway instanceof Sofort\Gateway) {
            $response = $gateway->completeAuthorize($params)->send();
        } else {
            $response = $gateway->completePurchase($params)->send();
        }

        return $response;
    }


    private function initGateway(GatewayConfigInterface $gatewayConfig): GatewayInterface
    {
        $gateway = Omnipay::create($gatewayConfig->getFactoryName());

        $params = $gatewayConfig->getConfig();
        if ($gateway instanceof Sofort\Gateway) {
            $e = explode(':', $params['config_key']);
            unset($params['config_key']);
            $params['username']  = $e[0];
            $params['projectId'] = $e[1];
            $params['password']  = $e[2];
        }
        $params['testMode'] = $gatewayConfig->isTestMode();
        $gateway->initialize($params);

        return $gateway;
    }
}
